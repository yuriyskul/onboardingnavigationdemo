package com.onboariding.demo.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.onboariding.demo.R


class WelocomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_welocome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val registerButton: Button = view.findViewById(R.id.register_button)
        registerButton.setOnClickListener {
            findNavController().navigate(R.id.action_welocomeFragment_to_explanationFragment)
        }

        val continueBtn: Button = view.findViewById(R.id.continue_button)
        continueBtn.setOnClickListener {
            finishOnboarding()
        }
    }

    private fun finishOnboarding() {
        val onboardingActivity = requireActivity() as OnboardingActivity
        onboardingActivity.finishOnboarding()
    }

}