package com.onboariding.demo.onboarding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.onboariding.demo.App
import com.onboariding.demo.HomeActivity
import com.onboariding.demo.R

class OnboardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (App.instance.appPrefs.isOnboardingFinished()) {
            HomeActivity.start(this)
            finish()
            return
        }
        setContentView(R.layout.activity_onboarding)


        val navHostFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        val navController = navHostFragment!!.findNavController()
        NavigationUI.setupActionBarWithNavController(this, navController)
    }


    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp()
    }

    fun finishOnboarding() {
        App.instance.appPrefs.setOnboardingIsFinished()
        HomeActivity.start(this)
        finish()
    }
}