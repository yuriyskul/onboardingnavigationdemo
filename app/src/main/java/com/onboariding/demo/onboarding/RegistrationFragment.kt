package com.onboariding.demo.onboarding

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.onboariding.demo.App
import com.onboariding.demo.R
import com.onboariding.demo.databinding.FragmentRegisterationBinding
import com.onboariding.demo.model.User


class RegistrationFragment : Fragment() {


    private var _binding: FragmentRegisterationBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//         Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_registeration, container, false)

        _binding = FragmentRegisterationBinding.inflate(inflater, container, false)


        binding.ageEditText.setOnEditorActionListener { _, keyCode, event ->
            if (((event?.action ?: -1) == KeyEvent.ACTION_DOWN)
                || keyCode == EditorInfo.IME_ACTION_DONE
            ) {
                onSaveClicked()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        binding.saveButton.setOnClickListener {
            onSaveClicked()
        }

        return binding.root
    }


    private fun onSaveClicked() {
        val name: String = binding.firstNameEditText.text.toString().trim()
        val lastName: String = binding.lastNameEditText.text.toString().trim()
        val age = binding.ageEditText.text.toString().trim()

        if (name.isBlank() || lastName.isBlank() || age.isBlank()) {
            // missing data - user has to update every registration form
            Toast.makeText(
                requireContext(),
                getString(R.string.update_every_input_form),
                Toast.LENGTH_LONG
            ).show()
        } else {
            saveUser(name, lastName, age)
        }
    }

    private fun saveUser(name: String, lastName: String, age: String) {
        val newUser = User(
            name = name,
            lastName = lastName,
            age = age.toInt()
        )
        App.instance.appPrefs.saveUser(newUser)
        finishOnboarding()
    }

    private fun finishOnboarding() {
        val onboardingActivity = requireActivity() as OnboardingActivity
        onboardingActivity.finishOnboarding()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}