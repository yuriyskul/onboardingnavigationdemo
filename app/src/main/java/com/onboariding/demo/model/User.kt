package com.onboariding.demo.model

import com.google.gson.annotations.Expose

data class User(
    @Expose
    val name: String,
    @Expose
    val lastName: String,
    @Expose
    val age: Int
)