package com.onboariding.demo.storage

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import com.onboariding.demo.model.User


class AppPrefs(appContext: Context) {

    private var prefs: SharedPreferences =
        appContext.getSharedPreferences(APP_PREFS_FILE_NAME, Context.MODE_PRIVATE)


    fun getUser(): User? {
        val gson = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        val json: String = prefs.getString(REGISTERED_USER, null) ?: return null
        return gson.fromJson(json, User::class.java)
    }

    fun saveUser(user: User) {
        val gson = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        val json = gson.toJson(user)
        val editor = prefs.edit()
        editor.putString(REGISTERED_USER, json)
        editor.apply()
    }


    fun isOnboardingFinished(): Boolean {
        return prefs.getBoolean(ONBOARDING_FINISHED, false)
    }

    fun setOnboardingIsFinished() {
        val editor = prefs.edit()
        editor.putBoolean(ONBOARDING_FINISHED, true)
        editor.apply()
    }


    companion object {
        private const val REGISTERED_USER = "registered_user"
        private const val ONBOARDING_FINISHED = "onboarding_finished"


        private val TAG = AppPrefs.javaClass.simpleName
        private val APP_PREFS_FILE_NAME = "app_prefs"
    }
}