package com.onboariding.demo

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class HomeActivity : AppCompatActivity() {

    private lateinit var helloTV: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        helloTV = findViewById(R.id.hello_text_view)

        val user = App.instance.appPrefs.getUser()

        user?.let {
            helloTV.text = "Hello ${user.name} ${user.lastName}"
        }
    }

    companion object {

        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, HomeActivity::class.java)
 //Intent.FLAG_ACTIVITY_CLEAR_TOP and Intent.FLAG_ACTIVITY_NEW_TASK clears the stack and makes it the top one.
            // So when we press back button the whole application will be terminated.
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivity(intent)
        }
    }
}