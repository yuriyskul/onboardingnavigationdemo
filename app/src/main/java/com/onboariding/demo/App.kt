package com.onboariding.demo

import android.app.Application
import com.onboariding.demo.storage.AppPrefs

class App : Application() {


    val appPrefs: AppPrefs by lazy {
        AppPrefs(instance)
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }


    companion object {
        lateinit var instance: App

        @JvmStatic
        fun get() = instance
    }
}